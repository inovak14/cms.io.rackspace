﻿Odporúčam nainštalovať si NCrunch, ktorý bude za Vás 
automaticky spúšťať unit testy pri každej zmene.
http://www.ncrunch.net/
Ak by ste to nechceli používať tak si z 
http://xunit.codeplex.com/ stiahnite xUnit a ten má 
v sebe aj GUI (a tiež command line tool) na spúšťanie 
testov.

Ako testovací framework som dal xUnit.net, pracuje 
sa s ním veľmi jednoducho. Vytvoril som aj testovací 
projekt s príkladom jednoduchého testu.
http://xunit.codeplex.com/wikipage?title=HowToUse&referringTitle=Home

Git asi máte keďže ste si dokázali toto stiahnúť, 
ale keby náhodou nie tak:
http://code.google.com/p/msysgit/downloads/detail?name=Git-1.7.11-preview20120710.exe&can=2&q=%22Full+installer+for+official+Git+for+Windows%22

Potom ak chcete tak si môžete nainštalovať TortoiseGit:
http://code.google.com/p/tortoisegit/wiki/Download

A celkom vhodná vec je aj Git Source Control provider 
do Visual Studia. 
Nainštalujete to cez Tools -> Extension Manager 
-> Online Gallery a dáte hľadať "Git"
http://gitscc.codeplex.com/