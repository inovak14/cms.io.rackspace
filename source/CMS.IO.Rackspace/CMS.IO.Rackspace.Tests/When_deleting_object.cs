﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_deleting_object
    {
        [Fact]
        public void No_exception_should_be_thrown()
        {
            IContainerFactory factory = new CFContainerFactory();
            var file = new File(factory);

            // act
            file.Delete("Test.txt");
                        
            // assert
            Assert.True(true);
        }
    }
}
