﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class File_info_demo
    {
        [Fact(Skip="Cant be tested automatically on appharbor")]
        public void No_exception_should_be_thrown()
        {
            
            IContainerFactory factory = new CFContainerFactory();
            FileInfo info = new FileInfo("Test.txt");

            // act
            
            // assert
            Assert.Equal(15, info.Length);
            Assert.Equal("txt", info.Extension);
            Assert.Equal("Test.txt", info.FullName);
            Assert.Equal("Test.txt", info.Name);
            Assert.Equal(true, info.Exists);
            
            info.MoveTo("Test_moved.txt");
            Assert.Equal("Test_moved.txt", info.Name);



        }
    }
}
