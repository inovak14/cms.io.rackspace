﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_creating_object
    {
        [Fact]
        public void No_exception_should_be_thrown()
        {
            // arrange
            IContainerFactory factory = new CFContainerFactory();
            var file = new File(factory);
            byte[] bytes = Encoding.ASCII.GetBytes("Testovaci subor");

            // act
            CMS.IO.FileStream fileStream = file.Create("Test.txt");
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Close();
        }
    }
}
