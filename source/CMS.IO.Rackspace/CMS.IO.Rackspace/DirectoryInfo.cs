﻿using System;

using CMS.IO;

namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of DirectoryInfo class object of CMS.IO provider.
    /// </summary>
    class DirectoryInfo : CMS.IO.DirectoryInfo
    {

        #region "Constructors"

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="path">Path to directory</param>
        public DirectoryInfo(string path)
        {
        }

        #endregion


        #region "Public properties"

        /// <summary>
        /// Full name of directory (whole path).
        /// </summary>
        public override string FullName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Last write time to directory.
        /// </summary>
        public override DateTime LastWriteTime
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Name of directory (without path).
        /// </summary>
        public override string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Creation time.
        /// </summary>
        public override DateTime CreationTime
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Whether directory exists.
        /// </summary>
        public override bool Exists
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Parent directory.
        /// </summary>
        public override CMS.IO.DirectoryInfo Parent
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion


        #region "Methods"

        /// <summary>
        /// Creates subdirectory.
        /// </summary>
        /// <param name="subdir">Subdirectory to create.</param>
        public override CMS.IO.DirectoryInfo CreateSubdirectory(string subdir)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Deletes directory
        /// </summary>
        public override void Delete()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns the subdirectories of the current directory.
        /// </summary>        
        public override CMS.IO.DirectoryInfo[] GetDirectories()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns the subdirectories of the current directory.
        /// </summary>
        /// <param name="searchPattern">Search pattern.</param>        
        public override CMS.IO.DirectoryInfo[] GetDirectories(string searchPattern)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns files of the current directory.
        /// </summary>        
        public override CMS.IO.FileInfo[] GetFiles()
        {
            throw new NotImplementedException(); ;
        }


        /// <summary>
        /// Returns files of the current directory.
        /// </summary>
        /// <param name="searchPattern">Search pattern.</param>        
        public override CMS.IO.FileInfo[] GetFiles(string searchPattern)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns files of the current directory.
        /// </summary>
        /// <param name="searchPattern">Search pattern.</param>
        /// <param name="searchOption">Whether return files from top directory or also from any subdirectories.</param>
        public override CMS.IO.FileInfo[] GetFiles(string searchPattern, CMS.IO.SearchOption searchOption)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
