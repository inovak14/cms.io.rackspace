﻿using System;
using System.Security.AccessControl;
using CMS.IO.Rackspace.Adapter;

using CMS.IO;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of Directory class of CMS.IO provider.
    /// </summary>
    public class Directory : CMS.IO.AbstractDirectory
    {
        #region "Public override methods"

        /// <summary>
        /// Determines whether the given path refers to an existing directory on disk.
        /// </summary>
        /// <param name="path">Path to test.</param>  
        public override bool Exists(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return false;
            if (!path.EndsWith("/"))
                return false;

            return new File(new CFContainerFactory()).Exists(path);
        }


        /// <summary>
        /// Creates all directories and subdirectories as specified by path.
        /// </summary>
        /// <param name="path">Path to create.</param> 
        public override CMS.IO.DirectoryInfo CreateDirectory(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns the names of files (including their paths) in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve files from.</param> 
        public override string[] GetFiles(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns the names of files (including their paths) in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve files from.</param> 
        /// <param name="searchPattern">Search pattern.</param>
        public override string[] GetFiles(string path, string searchPattern)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the names of subdirectories in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve directories from.</param> 
        public override string[] GetDirectories(string path)
        {
            return GetDirectories(path, "*");
        }


        /// <summary>
        /// Gets the names of subdirectories in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve directories from.</param> 
        /// <param name="searchPattern">Pattern to be searched.</param> 
        public override string[] GetDirectories(string path, string searchPattern)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets the current working directory of the application.
        /// </summary>  
        public override string GetCurrentDirectory()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Deletes an empty directory and, if indicated, any subdirectories and files in the directory.
        /// </summary>
        /// <param name="path">Path to directory</param>
        /// <param name="recursive">If delete if sub directories exists.</param>
        public override void Delete(string path, bool recursive)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Moves directory.
        /// </summary>
        /// <param name="sourceDirName">Source directory name.</param>
        /// <param name="destDirName">Destination directory name.</param>
        public override void Move(string sourceDirName, string destDirName)
        {
            if (sourceDirName == null)
                throw new ArgumentNullException("sourceDirName");
            if (destDirName == null)
                throw new ArgumentNullException("destDirName");

            if (String.IsNullOrWhiteSpace(sourceDirName))
                throw new ArgumentException("can't be blank", "sourceDirName");
            if (String.IsNullOrWhiteSpace(destDirName))
                throw new ArgumentException("can't be blank", "destDirName");

            // Invalid length of file names
            try
            {
                Common.ValidateObjectName(sourceDirName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded sourceDirName exceeds 1024 char's");
            }
            try
            {
                Common.ValidateObjectName(destDirName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded destDirName exceeds 1024 char's");
            }

            if (!Exists(sourceDirName))
                throw new System.IO.DirectoryNotFoundException(String.Format("{0} not found", sourceDirName));
            if (sourceDirName == destDirName)
                throw new System.IO.IOException("sourceDirName and destDirName refers to the same directory");
            if (Exists(destDirName))
                throw new System.IO.IOException(String.Format("{0} already exists", destDirName));

            try
            {
                CheckFilesPathLengthInNewDirectory(destDirName, sourceDirName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded file names exceeds 1024 char's");
            }

            File file = new File(new CFContainerFactory());
            file.Move(sourceDirName, destDirName);
            MoveSubDirectories(file, sourceDirName, destDirName);
        }

        private void MoveSubDirectories(File file, string sourceDirPath, string newDirPath)
        {
            if (file == null)
                throw new ArgumentNullException("file");
            if (newDirPath == null)
                throw new ArgumentNullException("newDirPath");
            if (sourceDirPath == null)
                throw new ArgumentNullException("sourceDirName");

            string[] files = GetFiles(sourceDirPath);
            foreach (string fileName in files)
            {
                file.Move(sourceDirPath + fileName, newDirPath + fileName);
            }

            string[] directories = GetDirectories(sourceDirPath);
            foreach (string directoryName in directories)
            {
                file.Move(sourceDirPath + directoryName, newDirPath + directoryName);
                MoveSubDirectories(file, sourceDirPath + directoryName, newDirPath + directoryName);
            }
        }
        /// <summary>
        /// It checks path length of files, which will be moved from directory to an another directory.
        /// </summary>
        /// <param name="nDir">current directory, which will be checked</param>
        /// <param name="cDir"></param>
        private void CheckFilesPathLengthInNewDirectory(string nDirPath, string cDirPath)
        {
            if (nDirPath == null)
                throw new ArgumentNullException("nDirPath");
            if (cDirPath == null)
                throw new ArgumentNullException("cDirName");

            string[] files = GetFiles(cDirPath);
            foreach (string file in files)
            {
                Common.ValidateObjectName(nDirPath + file);
            }

            string[] directories = GetDirectories(cDirPath);
            foreach (string directory in directories)
            {
                Common.ValidateObjectName(nDirPath + directory);
                CheckFilesPathLengthInNewDirectory(nDirPath + directory, cDirPath + directory);
            }
        }


        /// <summary>
        /// Deletes an empty directory.
        /// </summary>
        /// <param name="path">Path to directory</param>        
        public override void Delete(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets a FileSecurity object that encapsulates the access control list (ACL) entries for a specified file.
        /// </summary>
        /// <param name="path">Path to directory.</param>        
        public override DirectorySecurity GetAccessControl(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Prepares files for import. Converts them to media library.
        /// </summary>
        /// <param name="path">Path.</param>
        public override void PrepareFilesForImport(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Deletes all files in the directory structure. It works also in a shared hosting environment.
        /// </summary>
        /// <param name="path">Full path of the directory to delete</param>
        public override void DeleteDirectoryStructure(string path)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
