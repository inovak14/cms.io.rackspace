﻿using System;
using CMS.IO;
using Rackspace.Cloudfiles;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of FileInfo class of CMS.IO provider.
    /// </summary>
    public class FileInfo : CMS.IO.FileInfo
    {
        private string _filename;
        private readonly IContainerFactory _factory;
        private Container _container;
        private StorageObject _obj;

        #region "Constructors"

        /// <summary>
        /// Initializes new instance of
        /// </summary>
        /// <param name="filename">File name.</param>
        public FileInfo(string filename)
        {
            if (filename == null)
                throw new ArgumentNullException("filename");

            _filename = filename;
            _factory = new CFContainerFactory();
            _container = _factory.GetContainer();
            _obj = _container.GetObject(_filename);
        }

        #endregion


        #region "Properties"


        /// <summary>
        /// Length of file.
        /// </summary>
        public override long Length
        {
            get
            {
                return _obj.ContentLength;
            }
        }


        /// <summary>
        /// File extension.
        /// </summary>
        public override string Extension
        {
            get
            {
                return _obj.Name.Substring((_obj.Name.LastIndexOf('.')) + 1);
            }
        }


        /// <summary>
        /// Full name of file (with whole path).
        /// </summary>
        public override string FullName
        {
            get
            {
                return _filename;
            }
        }


        /// <summary>
        /// File name of file (without path).
        /// </summary>
        public override string Name
        {
            get
            {
                return _obj.Name;
            }
        }


        /// <summary>
        /// Last write time to file.
        /// </summary>
        public override DateTime LastWriteTime
        {
            get
            {
                File file = new File(_factory);
                return file.GetLastWriteTime(_filename);
            }
            set
            {
                File file = new File(_factory);
                file.SetLastWriteTime(_filename, DateTime.Now);
            }
        }


        /// <summary>
        /// If file exists.
        /// </summary>
        public override bool Exists
        {
            get
            {
                File file = new File(_factory);
                return file.Exists(_filename);
            }
        }


        /// <summary>
        /// Creation date of file.
        /// </summary>
        public override DateTime CreationTime
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Directory of file.
        /// </summary>
        public override CMS.IO.DirectoryInfo Directory
        {
            get
            {
                return new DirectoryInfo(_filename.Substring(0, _filename.LastIndexOf('/') + 1));
            }
        }


        /// <summary>
        ///  If is read only.
        /// </summary>
        public override bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// File attributes.
        /// </summary>
        public override CMS.IO.FileAttributes Attributes
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Directory name.
        /// </summary>
        public override string DirectoryName
        {
            get
            {
                return _filename.Substring(0, _filename.LastIndexOf('/') + 1);
            }
        }


        /// <summary>
        /// Last access time.
        /// </summary>
        public override DateTime LastAccessTime
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }        

        #endregion


        #region "Methods"

        /// <summary>
        /// Creates or opens a file for writing UTF-8 encoded text.
        /// </summary> 
        public override CMS.IO.StreamWriter CreateText()
        {
            File file = new File(_factory);
            return file.CreateText(_filename);
        }


        /// <summary>
        /// Deletes file.
        /// </summary>
        public override void Delete()
        {
            File file = new File(_factory);
            file.Delete(_filename);
        }


        /// <summary>
        /// Creates a read-only ICMSFileStream.
        /// </summary> 
        public override CMS.IO.FileStream OpenRead()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Copies current file to destination. 
        /// </summary>
        /// <param name="destFileName">Destination file name.</param>
        public override CMS.IO.FileInfo CopyTo(string destFileName)
        {
            if (destFileName == null)
                throw new ArgumentNullException("destFileName");
            if (String.IsNullOrWhiteSpace(destFileName))
                throw new ArgumentException("can't be blank", "destFileName");

            File file = new File(_factory);
            file.Copy(_filename, destFileName);

            return new FileInfo(destFileName);
        }


        /// <summary>
        /// Copies current file to destination. 
        /// </summary>
        /// <param name="destFileName">Destination file name.</param>
        /// <param name="overwrite">Indicates if existing file should be overwritten</param>        
        public override CMS.IO.FileInfo CopyTo(string destFileName, bool overwrite)
        {
            if (destFileName == null)
                throw new ArgumentNullException("destFileName");
            if (String.IsNullOrWhiteSpace(destFileName))
                throw new ArgumentException("can't be blank", "destFileName");

            File file = new File(_factory);
            file.Copy(_filename, destFileName, overwrite);
           
            return new FileInfo(destFileName);
        }


        /// <summary>
        /// Moves an existing file to a new file.
        /// </summary>
        /// <param name="destFileName">Destination file name.</param>        
        public override void MoveTo(string destFileName)
        {
            if (destFileName == null)
                throw new ArgumentNullException("destFileName");
            if (String.IsNullOrWhiteSpace(destFileName))
                throw new ArgumentException("can't be blank", "destFileName");

            File file = new File(_factory);
            file.Move(_filename, destFileName);
            _filename = destFileName;
            _obj = _container.GetObject(_filename);
        }


        /// <summary>
        /// Creates a StreamReader with UTF8 encoding that reads from an existing text file.
        /// </summary>  
        public override StreamReader OpenText()
        {
            File file = new File(_factory);
            return file.OpenText(_filename);
        }

        #endregion
    }
}
