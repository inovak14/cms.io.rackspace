﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace.Adapter
{
    internal class CFInfo
    {
        public const char DELIMITER = '/';
        public static readonly TimeSpan REFRESH_INTERVAL = TimeSpan.FromHours(23);
       
        private readonly Connection _connection;
        public Connection Connection
        {
            get { return _connection; }
        }

        private readonly string _containerName;
        public string ContainerName
        {
            get { return _containerName; }
        }

        public CFInfo()
        {
            var credentials = new UserCredentials(
                ConfigurationManager.AppSettings["CF_UserName"],
                ConfigurationManager.AppSettings["CF_ApiKey"]);
            _connection = new CF_Connection(credentials);
            _containerName = ConfigurationManager.AppSettings["CF_ContainerName"];
        }
    }
}
