﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rackspace.Cloudfiles;
using System.Configuration;

namespace CMS.IO.Rackspace.Adapter
{
    public class CFContainerFactory : IContainerFactory
    {
        private CFInfo _info;
        private DateTime _lastUpdatedAuthToken;
        private Container _container;

        public CFContainerFactory()
        {
            _info = new CFInfo();
            _lastUpdatedAuthToken = DateTime.MinValue;
        }

        private void Authenticate()
        {
            DateTime updateTime = _lastUpdatedAuthToken + 
                CFInfo.REFRESH_INTERVAL;
            if (DateTime.Now >= updateTime)
            {
                var connection = _info.Connection;
                var account = new CF_Account(connection);

                string containerName = _info.ContainerName;
                connection.Authenticate();
                _lastUpdatedAuthToken = DateTime.Now;
                try 
                {
                    _container = account.GetContainer(containerName);
                }
                catch (ContainerNotFoundException)
                {
                    _container = account.CreateContainer(containerName);
                }
            }
        }

        public Container GetContainer()
        {
            Authenticate();
            return _container;
        }
    }
}
