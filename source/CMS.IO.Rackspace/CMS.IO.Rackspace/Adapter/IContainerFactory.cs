﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace.Adapter
{
    public interface IContainerFactory
    {
        Container GetContainer();
    }
}
