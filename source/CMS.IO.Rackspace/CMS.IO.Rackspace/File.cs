﻿using System;
using System.Text;
using CMS.IO;

using System.Security.AccessControl;
using Rackspace.Cloudfiles;
using System.Configuration;
using CMS.IO.Rackspace.Adapter;
using System.Collections.Generic;

namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of File class of CMS.IO provider.
    /// </summary>
    public class File : CMS.IO.AbstractFile
    {
        private readonly IContainerFactory _adapterFactory;

        public File(IContainerFactory adapterFactory)
        {
            _adapterFactory = adapterFactory;
        }

        #region "Public properties"

        /// <summary>
        /// Determines whether the specified file exists.
        /// </summary>
        /// <param name="path">Path to file.</param>  
        public override bool Exists(string path)
        {
            Container container = _adapterFactory.GetContainer();

            try
            {
                container.GetObject(path);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Opens an existing UTF-8 encoded text file for reading.
        /// </summary>
        /// <param name="path">Path to file</param>
        public override CMS.IO.StreamReader OpenText(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Deletes the specified file. An exception is not thrown if the specified file does not exist.
        /// </summary>
        /// <param name="path">Path to file</param>
        public override void Delete(string path)
        {
            Container container = _adapterFactory.GetContainer();
            container.DeleteObject(path);
        }


        /// <summary>
        /// Copies an existing file to a new file. Overwriting a file of the same name is allowed.
        /// </summary>
        /// <param name="sourceFileName">Path to source file.</param>
        /// <param name="destFileName">Path to destination file.</param>
        /// <param name="overwrite">If destination file should be overwritten.</param>
        public override void Copy(string sourceFileName, string destFileName, bool overwrite)
        {
            if (sourceFileName == null)
                throw new ArgumentNullException("sourceFileName");
            if (destFileName == null)
                throw new ArgumentNullException("destFileName");

            if (String.IsNullOrWhiteSpace(sourceFileName))
                throw new ArgumentException("can't be blank", "sourceFileName");
            if (String.IsNullOrWhiteSpace(destFileName))
                throw new ArgumentException("can't be blank", "destFileName");

            // Invalid length of file names
            try
            {
                Common.ValidateObjectName(sourceFileName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded sourceFileName exceeds 1024 char's");
            }
            try
            {
                Common.ValidateObjectName(destFileName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded destFileName exceeds 1024 char's");
            }

            // Directory not found
            if (sourceFileName.Contains("/"))
	        {
                if (!new Directory().Exists(sourceFileName.Substring(0, sourceFileName.LastIndexOf('/') + 1)))
                {
                    throw new System.IO.DirectoryNotFoundException(String.Format("{0} directory not found", sourceFileName));
                }
	        }
            if (destFileName.Contains("/"))
            {
                if (!new Directory().Exists(destFileName.Substring(0, destFileName.LastIndexOf('/') + 1)))
                {
                    throw new System.IO.DirectoryNotFoundException(String.Format("{0} directory not found", destFileName));
                }
            }

            // sourceFileName not found
            if (!Exists(sourceFileName))
            {
                throw new System.IO.FileNotFoundException("not found", sourceFileName);
            }
            if (Exists(destFileName) && !overwrite)
            {
                throw new System.IO.IOException();
            }

            Container container = _adapterFactory.GetContainer();
            try
            {
                container.CopyObject(sourceFileName, destFileName);
            }
            catch (Exception e)
            {
                throw new System.IO.IOException(e.Message);
            }
        }


        /// <summary>
        /// Copies an existing file to a new file. Overwriting a file of the same name is not allowed.
        /// </summary>
        /// <param name="sourceFileName">Path to source file.</param>
        /// <param name="destFileName">Path to destination file.</param>        
        public override void Copy(string sourceFileName, string destFileName)
        {
            Copy(sourceFileName, destFileName, false);
        }


        /// <summary>
        /// Opens a binary file, reads the contents of the file into a byte array, and then closes the file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override byte[] ReadAllBytes(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Creates or overwrites a file in the specified path.
        /// </summary>
        /// <param name="path">Path to file.</param> 
        public override CMS.IO.FileStream Create(string path)
        {
            Container container = _adapterFactory.GetContainer();
            StorageObject obj = container.CreateObject(path);
            return new FileStream(path, obj);
        }


        /// <summary>
        /// Moves a specified file to a new location, providing the option to specify a new file name.
        /// </summary>
        /// <param name="sourceFileName">Source file name.</param>
        /// <param name="destFileName">Destination file name.</param>
        public override void Move(string sourceFileName, string destFileName)
        {
            if (sourceFileName == null)
                throw new ArgumentNullException("sourceFileName");
            if (destFileName == null)
                throw new ArgumentNullException("destFileName");

            if (String.IsNullOrWhiteSpace(sourceFileName))
                throw new ArgumentException("can't be blank", "sourceFileName");
            if (String.IsNullOrWhiteSpace(destFileName))
                throw new ArgumentException("can't be blank", "destFileName");

            // Invalid length of file names
            try
            {
                Common.ValidateObjectName(sourceFileName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded sourceFileName exceeds 1024 char's");
            }
            try
            {
                Common.ValidateObjectName(destFileName);
            }
            catch (ObjectNameException)
            {
                throw new System.IO.PathTooLongException("Url Encoded destFileName exceeds 1024 char's");
            }

            // Directory not found
            if (sourceFileName.Contains("/"))
            {
                if (!new Directory().Exists(sourceFileName.Substring(0, sourceFileName.LastIndexOf('/') + 1)))
                {
                    throw new System.IO.DirectoryNotFoundException(String.Format("{0} directory not found", sourceFileName));
                }
            }
            if (destFileName.Contains("/"))
            {
                if (!new Directory().Exists(destFileName.Substring(0, destFileName.LastIndexOf('/') + 1)))
                {
                    throw new System.IO.DirectoryNotFoundException(String.Format("{0} directory not found", destFileName));
                }
            }
            
            // sourceFileName not found
            if (!Exists(sourceFileName))
            {
                throw new System.IO.IOException(String.Format("{0} not found", sourceFileName));
            }
            if (Exists(destFileName))
            {
                throw new System.IO.IOException(String.Format("{0} already exists", destFileName));
            }

            Copy(sourceFileName, destFileName);
            Delete(sourceFileName);
        }


        /// <summary>
        /// Opens a text file, reads all lines of the file, and then closes the file.
        /// </summary>
        /// <param name="path">Path to file.</param> 
        public override string ReadAllText(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Creates a new file, write the contents to the file, and then closes the file. If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <param name="contents">Content to write.</param>
        public override void WriteAllText(string path, string contents)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Opens a file, appends the specified string to the file, and then closes the file. If the file does not exist, this method creates a file, writes the specified string to the file, then closes the file. 
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="contents">Content to write.</param>
        public override void AppendAllText(string path, string contents)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Opens a file, appends the specified string to the file, and then closes the file. If the file does not exist, this method creates a file, writes the specified string to the file, then closes the file. 
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="contents">Content to write.</param>
        /// <param name="encoding">The character encoding to use</param>
        public override void AppendAllText(string path, string contents, Encoding encoding)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Creates a new file, writes the specified byte array to the file, and then closes the file. If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="bytes">Bytes to write.</param>
        public override void WriteAllBytes(string path, byte[] bytes)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Opens an existing file for reading.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override CMS.IO.FileStream OpenRead(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Sets the specified FileAttributes  of the file on the specified path.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="fileAttributes">File attributes.</param>
        public override void SetAttributes(string path, CMS.IO.FileAttributes fileAttributes)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Opens a FileStream  on the specified path, with the specified mode and access.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="mode">File mode.</param>
        /// <param name="access">File access.</param>
        public override CMS.IO.FileStream Open(string path, CMS.IO.FileMode mode, CMS.IO.FileAccess access)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Sets the date and time, in coordinated universal time (UTC), that the specified file was last written to.
        /// </summary>
        /// <param name="path">Path.</param>
        /// <param name="lastWriteTimeUtc">Specified time.</param>
        public override void SetLastWriteTimeUtc(string path, DateTime lastWriteTimeUtc)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Creates or opens a file for writing UTF-8 encoded text.
        /// </summary>
        /// <param name="path">Path to file.</param>      
        public override CMS.IO.StreamWriter CreateText(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets a FileSecurity object that encapsulates the access control list (ACL) entries for a specified directory.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override FileSecurity GetAccessControl(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns the date and time the specified file or directory was last written to.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override DateTime GetLastWriteTime(string path)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Sets the date and time that the specified file was last written to.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="lastWriteTime">Last write time.</param>
        public override void SetLastWriteTime(string path, DateTime lastWriteTime)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns actual URL of given absolute path.
        /// </summary>
        /// <param name="absolutePath">Absolute path.</param>
        [Obsolete("Use GetFileUrl(absolutePath, siteName) instead.")]
        public override string GetActualUrl(string absolutePath)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Returns URL to file. If can be accessed directly then direct URL is generated else URL with GetFile page is generated.
        /// </summary>
        /// <param name="path">Virtual path starting with ~ or absolute path.</param>
        /// <param name="siteName">Site name.</param>
        public override string GetFileUrl(string path, string siteName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
